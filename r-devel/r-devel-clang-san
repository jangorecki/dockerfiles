FROM debian:testing

LABEL org.opencontainers.image.licenses="GPL-2.0-or-later" \
      maintainer="Jan Gorecki <j.gorecki@wit.edu.pl>"

ENV DEBIAN_FRONTEND=noninteractive

# libclang-rt-dev contains the static sanitizer libraries; llvm contains the symbolizer
RUN apt-get -qq update \
  && apt-get install -y --no-install-recommends \
    make \
    clang libclang-rt-dev llvm \
    clang-tidy \
    ca-certificates \
    wget \
  && rm -rf /var/lib/apt/lists/*

RUN echo "deb http://http.debian.net/debian sid main" > /etc/apt/sources.list.d/debian-unstable.list \
  && echo "deb-src http://http.debian.net/debian sid main" > /etc/apt/sources.list.d/debian-unstable.list

# otherwise executables built with -fsanitize=address (=> LeakSanitizer) might fail
ENV ASAN_OPTIONS=detect_leaks=0

RUN apt-get -qq update \
  && apt-get install -y --no-install-recommends equivs devscripts \
  && mk-build-deps r-base \
  && cp /usr/bin/checkbashisms /tmp/checkbashisms \
  && apt-get purge -y equivs devscripts \
  && mv /tmp/checkbashisms /usr/bin/checkbashisms \
  && apt-get install -y --no-install-recommends /r-base-build-deps_*.deb \
  && rm r-base-build-deps_* \
  && wget https://stat.ethz.ch/R/daily/R-devel.tar.gz \
  && tar -xf R-devel.tar.gz \
  && rm R-devel.tar.gz \
  && cd /R-devel \
  && R_PAPERSIZE=letter \
    R_BATCHSAVE="--no-save --no-restore" \
    R_BROWSER=xdg-open \
    PAGER=/usr/bin/pager \
    PERL=/usr/bin/perl \
    R_UNZIPCMD=/usr/bin/unzip \
    R_ZIPCMD=/usr/bin/zip \
    R_PRINTCMD=/usr/bin/lpr \
    LIBnn=lib \
    AWK=/usr/bin/awk \
    CC='/usr/bin/clang -fsanitize=address,undefined -fno-omit-frame-pointer' \
    CXX='/usr/bin/clang++ -fsanitize=address,undefined -fno-omit-frame-pointer' \
    ./configure \
      --without-recommended-packages \
  && make \
  && apt-get purge -y r-base-build-deps \
  && apt-get autoremove -y \
  && apt-get install -y r-base \
  && R --version > /runtime-deps-r-base-version \
  && apt-get purge -y r-base r-base-core r-recommended r-cran-* \
  && make install \
  && cd / \
  && apt-get install -y --no-install-recommends \
    tidy \
    pandoc \
    qpdf \
    pkg-config \
    perl \
    texinfo \
    locales \
  && apt-get clean \
  && apt-get autoclean \
  && rm -rf /var/lib/apt/lists/* /R-devel \
  && echo 'options("repos"="https://cloud.r-project.org")' >> /usr/local/lib/R/etc/Rprofile.site \
  && wget -q https://yihui.org/tinytex/install-bin-unix.sh \
  && sh install-bin-unix.sh \
  && rm install-bin-unix.sh

RUN dpkg-reconfigure locales \
    && locale-gen C.UTF-8 \
    && /usr/sbin/update-locale LANG=C.UTF-8

ENV LC_ALL=C.UTF-8
ENV PATH="${PATH}:/root/.TinyTeX/bin/x86_64-linux"

CMD ["R"]
